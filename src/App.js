import './App.css';
import BurgerConstructor from "./containers/BurgerConstructor/BurgerConstructor";

const App = () => (
    <div className="App">
        <BurgerConstructor/>
    </div>
);

export default App;
