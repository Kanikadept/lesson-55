import React, {useState} from 'react';
import Ingredients from "../../components/Ingredients/Ingredients";
import Burger from "../../components/Burger/Burger";
import './BurgerContructor.css'


const BurgerConstructor = () => {

    const [ingredients, setIngredients] = useState([
        {id: 1, name: 'Meat', count: 0, price: 50},
        {id: 2, name: 'Cheese', count: 0, price: 20},
        {id: 3, name: 'Salad', count: 0, price: 5},
        {id: 4, name: 'Bacon', count: 0, price: 30}
    ]);

    const addIngredient = (ingredientId, event) => {
        event.stopPropagation();
        const index = ingredients.findIndex(ingredients => ingredients.id === ingredientId);
        const ingredientsCopy = [...ingredients];
        ingredientsCopy[index].count++;

        setIngredients(ingredientsCopy);
    }

    const removeIngredient = (ingredientId, event) => {
        event.stopPropagation();
        const index = ingredients.findIndex(ingredients => ingredients.id === ingredientId);
        const ingredientsCopy = [...ingredients];
        ingredientsCopy[index].count = 0;

        setIngredients(ingredientsCopy);
    }

    return (
        <div className="Burger-contructor">
            <Ingredients ingredientsArr={ingredients}
                         addIngredient={addIngredient}
                         removeIngredient={removeIngredient}/>
            <Burger ingredientsArr={ingredients} />
        </div>
    );
};

export default BurgerConstructor;