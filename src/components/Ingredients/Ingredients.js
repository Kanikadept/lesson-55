import React from 'react';
import './Ingredient.css';

const Ingredients = props => {

    return (
        <div className="ingredients">
            {props.ingredientsArr.map(ingredient => {
                return <div key={ingredient.id}>
                            <button onClick={(event) => props.addIngredient(ingredient.id, event)}>
                                Choose</button> {ingredient.name} x{ingredient.count}
                            <button onClick={(event) => props.removeIngredient(ingredient.id, event)}>delete</button>
                       </div>
            })}
        </div>
    );
};

export default Ingredients;