import React from 'react';
import './Burger.css';
import { nanoid } from 'nanoid';

const Burger = props => {
    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            <>
                {props.ingredientsArr.map(ingredient => {
                    let ingred = [];
                    for(let i = 0; i < ingredient.count; i++) {
                        ingred.push(<div key={nanoid()} className={ingredient.name}></div>);
                    }
                    return ingred;
                })}
                <div className="BreadBottom"></div>
            </>
            <p>Price: {props.ingredientsArr.reduce((acc, ingredient) => {
                acc += ingredient.price * ingredient.count;
                return acc;
            }, 0)}</p>
        </div>
    );
};

export default Burger;